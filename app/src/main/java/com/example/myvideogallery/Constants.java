package com.example.myvideogallery;

/**
 * Created by Gauthier on 25/07/16.
 */
public class Constants {


    public static final String VIDEO_DIRECTORY = "MyVideoGallery";
    public static final int MAX_VIDEO_DURATION = 20000;

    // The title of a video has a lot of information in it. To decode it we use defined separator
    public static final String TITLE_SEPARATOR = "_";
    public static final String TITLE_FILE_SPACE = "-";
    public static final String TITLE_READABLE_SPACE = " ";
}
