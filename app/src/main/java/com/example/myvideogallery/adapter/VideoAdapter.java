package com.example.myvideogallery.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.myvideogallery.MainActivity;
import com.example.myvideogallery.R;
import com.example.myvideogallery.model.Video;
import com.example.myvideogallery.util.MiscHelper;
import com.example.myvideogallery.util.VideoRequestHandler;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Gauthier on 25/07/16.
 */
public class VideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private MainActivity mainActivity;
    public ArrayList<Video> videoList;

    // Thank you Jake for Picasso
    VideoRequestHandler videoRequestHandler;
    Picasso picassoInstance;

    /**
     *
     * Adapter that loads Video object data into item_video layout
     */
    public VideoAdapter(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        videoList = new ArrayList<>();

        // This will let us load/cache thumbnail of the videos
        videoRequestHandler=new VideoRequestHandler();
        picassoInstance = new Picasso.Builder(mainActivity.getApplicationContext())
                .addRequestHandler(videoRequestHandler)
                .build();
    }


    public class VideoItemHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.itemVideoRoot) RelativeLayout itemVideoRoot;
        @BindView(R.id.image_video) ImageView image;
        @BindView(R.id.text_title) TextView text_title;
        @BindView(R.id.text_timestamp) TextView text_timestamp;
        @BindView(R.id.text_duration) TextView text_duration;
        @BindView(R.id.button_delete) Button button_delete;
        @BindView(R.id.button_share) Button button_share;
        public VideoItemHolder(View v){
            super(v);
            ButterKnife.bind(this, v);
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_video, parent, false);
        return new VideoItemHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final Video video = videoList.get(position);
        VideoItemHolder videoItemHolder = (VideoItemHolder) holder;

        picassoInstance.load(videoRequestHandler.SCHEME_VIEDEO+":"+video.path).fit().centerCrop().into(videoItemHolder.image);
        videoItemHolder.itemVideoRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.showVideoPlayerFragment(video.path);
            }
        });
        videoItemHolder.text_title.setText(video.title);

        videoItemHolder.text_timestamp.setText(video.timestamp);

        videoItemHolder.text_duration.setText(video.duration);

        videoItemHolder.button_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new File(video.path).delete();
                videoList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, getItemCount());
            }
        });

        videoItemHolder.button_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MiscHelper.shareVideo((MainActivity)mainActivity, video.path);
            }
        });
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }
}
