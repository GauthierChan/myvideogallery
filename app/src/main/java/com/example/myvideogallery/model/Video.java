package com.example.myvideogallery.model;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.support.annotation.NonNull;

import com.example.myvideogallery.Constants;
import com.example.myvideogallery.R;
import com.example.myvideogallery.util.MiscHelper;

import java.io.File;

/**
 * Created by Gauthier on 25/07/16.
 */
public class Video implements Comparable<Video>{

    public String path; // Absolute path to the video
    public String title; // the title goes like : Video x. According to the number of video the user took
    public String timestamp;
    public String duration;

    public long comparableTimestamp; // We use this to compare the date of 2 videos

    // We get all info from the path of the file
    // File name: VID_25072016_151136_Video-1.mp4
    // We split in order to set our fields
    public Video(Context context, String absolutePath, String title) {
        this.path = absolutePath + File.separator + title;

        String[] explodedTitle = title.split(Constants.TITLE_SEPARATOR);

        this.title = explodedTitle[3].replace(Constants.TITLE_FILE_SPACE, Constants.TITLE_READABLE_SPACE).replace(".mp4", "");

        String tempTimestamp = explodedTitle[1]+ Constants.TITLE_SEPARATOR +explodedTitle[2];
        timestamp = MiscHelper.getReadableTimestamp(tempTimestamp);
        comparableTimestamp = MiscHelper.parseLongTimestamp(tempTimestamp);


        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(absolutePath+File.separator+title);
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInmillisec = Long.parseLong( time );
        int seconds = (int) (timeInmillisec / 1000);

        this.duration = String.valueOf(seconds) + Constants.TITLE_READABLE_SPACE + context.getResources().getQuantityString(R.plurals.second, seconds);

    }

    @Override
    public int compareTo(@NonNull Video v) {
        return (int) (v.comparableTimestamp - comparableTimestamp);
    }
}
