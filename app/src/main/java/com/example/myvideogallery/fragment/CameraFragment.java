package com.example.myvideogallery.fragment;

import android.content.Context;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myvideogallery.Constants;
import com.example.myvideogallery.MainActivity;
import com.example.myvideogallery.R;
import com.example.myvideogallery.camera.CameraHelper;
import com.example.myvideogallery.camera.CameraPreview;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Gauthier on 25/07/16.
 */
public class CameraFragment extends Fragment {

    public static final String TAG = "CameraFragment";

    private String videoTitle;

    private Camera mCamera;
    private CameraPreview mPreview;
    private MediaRecorder mMediaRecorder;
    private boolean isRecording = false;
    private CountDownTimer mCountDownTimer;

    @BindView(R.id.camera_preview) FrameLayout preview;
    @BindView(R.id.button_back) Button backButton;
    @BindView(R.id.button_capture) Button captureButton;
    @BindView(R.id.text_title) TextView text_title;
    @BindView(R.id.text_countdown) TextView text_countdown;

    public static CameraFragment newInstance(String videoTitle) {
        CameraFragment f = new CameraFragment();
        Bundle args = new Bundle();
        args.putString("videoTitle", videoTitle);
        f.setArguments(args);
        return f;
    }

    /**
     *
     * The CameraFragment contains a SurfaceView that gives the user a preview of what the Camera sees.
     * It also contains different elements of interaction so that the user can navigate, start a recording,
     * stop it.
     * There's also indication with the title of the current video, and a countdown to know when the recording will stop
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null){
            videoTitle = getArguments().getString("videoTitle");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = (View) inflater.inflate(R.layout.fragment_camera, null);
        ButterKnife.bind(this, rootView);

        text_title.setText(videoTitle);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).getSupportFragmentManager().popBackStackImmediate();
            }
        });

        captureButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isRecording) {
                            // Save the record and pop this fragment from the backstack
                            finish();

                        } else {
                            // initialize video camera
                            if (prepareVideoRecorder()) {
                                // Camera is available and unlocked, MediaRecorder is prepared,
                                // now you can start recording
                                mMediaRecorder.start();

                                mCountDownTimer = new CountDownTimer(Constants.MAX_VIDEO_DURATION, 1000) {

                                    public void onTick(long millisUntilFinished) {
                                        text_countdown.setText(String.valueOf(+millisUntilFinished / 1000));
                                    }

                                    public void onFinish() {
                                        text_countdown.setVisibility(View.INVISIBLE);
                                    }
                                }.start();
                                text_countdown.setVisibility(View.VISIBLE);
                                // inform the user that recording has started
                                captureButton.setText("Stop");
                                isRecording = true;
                            } else {
                                // prepare didn't work, release the camera
                                releaseMediaRecorder();
                                // inform user
                            }
                        }
                    }
                }
        );
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        isRecording = false;
        text_countdown.setVisibility(View.INVISIBLE);
        captureButton.setText("Capture");

        // Create an instance of Camera
        mCamera = CameraHelper.getCameraInstance();

        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(getContext(), mCamera);

        preview.addView(mPreview);

    }

    @Override
    public void onPause() {
        super.onPause();
        releaseMediaRecorder();       // if you are using MediaRecorder, release it first
        releaseCamera();              // release the camera immediately on pause event
        if(mCountDownTimer != null) mCountDownTimer.cancel();
    }
    private void releaseMediaRecorder(){
        if (mMediaRecorder != null) {
            mMediaRecorder.reset();   // clear recorder configuration
            mMediaRecorder.release(); // release the recorder object
            mMediaRecorder = null;
            mCamera.lock();           // lock camera for later use
        }
    }

    private void releaseCamera(){
        if (mCamera != null){
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
        mPreview.getHolder().removeCallback(mPreview);
    }

    public void finish(){
        // stop recording and release camera
        try{
            mMediaRecorder.stop();
        }catch(RuntimeException stopException){
            //handle cleanup here
        }
        releaseMediaRecorder(); // release the MediaRecorder object
        mCamera.lock();         // take camera access back from MediaRecorder

        // inform the user that recording has stopped
        captureButton.setText("Capture");
        isRecording = false;
        Toast.makeText((MainActivity)getActivity(), "Saved", Toast.LENGTH_SHORT).show();
        ((MainActivity)getActivity()).refreshList();
        ((MainActivity)getActivity()).getSupportFragmentManager().popBackStackImmediate();
    }



    private boolean prepareVideoRecorder(){

//        mCamera = CameraHelper.getCameraInstance();
        mMediaRecorder = new MediaRecorder();

        // Step 1: Unlock and set camera to MediaRecorder
        mCamera.unlock();
        mMediaRecorder.setCamera(mCamera);

        // Step 2: Set sources
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.MPEG_4_SP);

        // Step 3: Set output file
        mMediaRecorder.setOutputFile(CameraHelper.getOutputMediaFile(videoTitle).toString());

        // Step 4: Set the preview output
        mMediaRecorder.setPreviewDisplay(mPreview.getHolder().getSurface());

        // Step 5: Prepare configured MediaRecorder
        mMediaRecorder.setOrientationHint(getOrientationHint());
        mMediaRecorder.setMaxDuration(Constants.MAX_VIDEO_DURATION);
        mMediaRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
            @Override
            public void onInfo(MediaRecorder mr, int what, int extra) {
                if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
                    finish();
                }
            }
        });
        try {
            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
            Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    private int getOrientationHint() {
        Display display = ((WindowManager)getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

        if(display.getRotation() == Surface.ROTATION_0)
            return 90;

        if(display.getRotation() == Surface.ROTATION_90)
            return 180;

        if(display.getRotation() == Surface.ROTATION_180)
            return 180;

        if(display.getRotation() == Surface.ROTATION_270)
            return 90;

        return 0;
    }
}
