package com.example.myvideogallery.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.myvideogallery.MainActivity;
import com.example.myvideogallery.util.MiscHelper;
import com.example.myvideogallery.R;
import com.example.myvideogallery.adapter.VideoAdapter;

import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Gauthier on 25/07/16.
 */
public class MainFragment extends Fragment {

    public static final String TAG = "MainFragment";

    LinearLayoutManager linearLayoutManager;
    VideoAdapter videoAdapter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    /**
     *
     * The MainFragment consists of a RecyclerView that contains the videos,
     * and a menu item that let the user go to the CameraFragment to record.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // retain this fragment
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = (View) inflater.inflate(R.layout.fragment_main, null);
        ButterKnife.bind(this, rootView);

        // Init toolbar
        ((MainActivity)getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);

        // Init RecyclerView
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        videoAdapter = new VideoAdapter((MainActivity) getActivity());
        videoAdapter.videoList = MiscHelper.getVideoList(getContext());
        Collections.sort(videoAdapter.videoList);

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(videoAdapter);
        videoAdapter.notifyDataSetChanged();

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_activity_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_record:
                ((MainActivity)getActivity()).showCameraFragment();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void refreshList(){
        videoAdapter.videoList = MiscHelper.getVideoList(getContext());
        Collections.sort(videoAdapter.videoList);
        videoAdapter.notifyDataSetChanged();
    }
}
