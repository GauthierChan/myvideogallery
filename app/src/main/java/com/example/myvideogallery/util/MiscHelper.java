package com.example.myvideogallery.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.example.myvideogallery.Constants;
import com.example.myvideogallery.model.Video;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Gauthier on 25/07/16.
 */
public class MiscHelper {

    // Returns an ArrayList containing all the video within the defined saving directory
    public static ArrayList<Video> getVideoList(Context context){
        String[] fileList = null;
        ArrayList<Video> videoArrayList = new ArrayList<>();
        File videoFiles = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), Constants.VIDEO_DIRECTORY);

        System.out.println("is directory : " + videoFiles.isDirectory());
        if(videoFiles.isDirectory())
        {
            fileList=videoFiles.list();
            if(fileList == null) return new ArrayList<>();
            System.out.println("file : " + fileList.length);
        }

        for(int i=fileList.length-1;i>=0;i--)
        {
            Log.i("Video:" + i + " File name", fileList[i]);
            videoArrayList.add(new Video(context, videoFiles.getAbsolutePath(), fileList[i]));
        }
        return videoArrayList;
    }


    // Share a video by making sure it has been scanned before (otherwise facebook, twitter, GMAIL
    // don't accept our content)
    public static void shareVideo(final Activity activity, String path){
        MediaScannerConnection.scanFile(activity, new String[]{path}, null,
            new MediaScannerConnection.OnScanCompletedListener() {
                public void onScanCompleted(String path, Uri uri) {
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("video/mp4");
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "My awesome video");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, ":smirk:");
                    sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
                    activity.startActivity(Intent.createChooser(sharingIntent, "Share video using"));
                }
            });
    }


    // ie : changes "25072016_154344" into "15:43:44, 25 Jul"
    public static String getReadableTimestamp(String timestamp){
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.US);
        Date d = null;
        try {
            d = sdf.parse(timestamp);
        } catch (ParseException e) {
            e.printStackTrace();
            return timestamp;
        }

        return new SimpleDateFormat("HH:mm:ss, dd MMM", Locale.US).format(d);
    }


    // ie : change "25072016_154344" into a super long value that represent the time elapsed since 1970 and the timestamp in ms..
    public static Long parseLongTimestamp(String timestamp){
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.US);
        Date d = null;
        try {
            d = sdf.parse(timestamp);
        } catch (ParseException e) {
            e.printStackTrace();
            return 0L; // By default we retrieve the oldest time possible
        }
        return d.getTime();
    }

}
