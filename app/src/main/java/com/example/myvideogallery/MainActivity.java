package com.example.myvideogallery;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.myvideogallery.dialog.PermissionDialog;
import com.example.myvideogallery.dialog.VideoPlayerDialogFragment;
import com.example.myvideogallery.fragment.CameraFragment;
import com.example.myvideogallery.fragment.MainFragment;
import com.example.myvideogallery.util.PermissionHelper;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences sharedPref;

    private int getContainer(){
        return R.id.mainActivityContainer;
    }

    /**
     *
     * MainActivity consists of an empty container in which we will manipulate Fragments.
     *
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        MainFragment tempMainFragment = (MainFragment) getSupportFragmentManager().findFragmentByTag(MainFragment.TAG);
        if(tempMainFragment == null)showMainFragment();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Instead of checking if every permission are granted
        // We look if one permission is not granted.
        boolean hasAllPermissionsRequired = true;
        for (int grantResult : grantResults) {
            if (grantResult == PackageManager.PERMISSION_DENIED)
                hasAllPermissionsRequired = false;
        }

        // If we received all the permissions, we call again the method to show the CameraFragment
        if(hasAllPermissionsRequired){
            switch (requestCode){
                case PermissionHelper.RECORDING_PERMISSIONS_REQUEST :
                    showCameraFragment();
                    break;
            }
        }
    }


    // ------------------------------------------------------------------------ FRAGMENT TRANSACTION

    public void showMainFragment(){
        Fragment newFragment = new MainFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(getContainer(), newFragment, MainFragment.TAG)
                .commit();
    }

    public void showCameraFragment(){
        // We check if we have the permission to use the Camera and save files
        if(PermissionHelper.hasPermissionForRecording(this)) {
            Fragment newFragment = CameraFragment.newInstance("Video-" + getCurrentVideoCount());
            setCurrentVideoCount(getCurrentVideoCount() + 1);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(getContainer(), newFragment)
                    .addToBackStack(CameraFragment.TAG)
                    .commitAllowingStateLoss();
        }
        else{
            // Otherwise we show the user a dialog explaining for what reasons we ask permissions
            new PermissionDialog().with("Hey there",
                "In order to create awesome videos, we need to ask you some permissions first",
                "Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA,}, PermissionHelper.RECORDING_PERMISSIONS_REQUEST);
                    }
                },
                "Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show(getSupportFragmentManager(), PermissionDialog.TAG);

        }
    }

    public void showVideoPlayerFragment(String path){
        VideoPlayerDialogFragment newFragment = VideoPlayerDialogFragment.newInstance(path);

        newFragment.show(getSupportFragmentManager().beginTransaction(), VideoPlayerDialogFragment.TAG);
    }

    /**
     * Refreshes the list of Videos in the RecyclerView of the MainFragment
     */
    public void refreshList(){
        ((MainFragment) getSupportFragmentManager().findFragmentByTag(MainFragment.TAG)).refreshList();
    }

    // ---------------------------------------------------------------------------------------------

    // -------------------------------------------------------------------------- SHARED PREFERENCES
    public SharedPreferences getSharedPreference(int mode){
        if(sharedPref == null){
            sharedPref = getSharedPreferences(getString(R.string.preference_file_key), mode);
        }
        return sharedPref;
    }

    public int getCurrentVideoCount(){
        SharedPreferences sharedPref = getSharedPreference(Context.MODE_PRIVATE);
        return sharedPref.getInt(getString(R.string.preference_video_count), 1);
    }

    public void setCurrentVideoCount(int value){
        SharedPreferences.Editor editor = getSharedPreference(Context.MODE_PRIVATE).edit();
        editor.putInt(getString(R.string.preference_video_count), value);
        editor.apply();
    }

    // ---------------------------------------------------------------------------------------------

}
