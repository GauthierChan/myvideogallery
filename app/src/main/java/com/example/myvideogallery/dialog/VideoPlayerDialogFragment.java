package com.example.myvideogallery.dialog;

import android.app.Dialog;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.VideoView;

import com.example.myvideogallery.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Gauthier on 25/07/16.
 */
public class VideoPlayerDialogFragment extends DialogFragment {

    public static final String TAG = "VideoPlayerDialogFragment";

    private String path;
    private int stopPosition;

    @BindView(R.id.video_view) VideoView videoView;
    private View rootView;

    /**
     *
     * VideoPlayerDialogFragment is, can you guess it ?, a dialog fragment that shows a video.
     * It handles lifecycle changes in order to pause/resume at the right moments.
     * It also loops.
     */
    public static VideoPlayerDialogFragment newInstance(String path) {
        VideoPlayerDialogFragment f = new VideoPlayerDialogFragment();
        Bundle args = new Bundle();
        args.putString("path", path);
        f.setArguments(args);
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = (View) inflater.inflate(R.layout.fragment_video_player, null);
        ButterKnife.bind(this, rootView);
        if (getArguments()!=null){
            path = getArguments().getString("path");
            stopPosition = getArguments().getInt("position");
        }

        // The VideoView will be dimmed by the Dialog otherwise
        videoView.setZOrderOnTop(true);

        videoView.setVideoPath(path);

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                videoView.start();
            }
        });

        return rootView;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState!=null){
            stopPosition = getArguments().getInt("position");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        videoView.seekTo(stopPosition);
        videoView.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopPosition = videoView.getCurrentPosition();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        videoView.pause();
        outState.putInt("position", stopPosition);
    }
}
