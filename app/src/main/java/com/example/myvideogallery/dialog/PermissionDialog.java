package com.example.myvideogallery.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

/**
 * Created by Gauthier on 25/07/16.
 */
public class PermissionDialog extends AppCompatDialogFragment {
    public static final String TAG = "PermissionDialog";


    private String title;
    private String description;
    private String positiveText;
    private DialogInterface.OnClickListener positiveClickListener;
    private String negativeText;
    private DialogInterface.OnClickListener negativeClickListener;

    public PermissionDialog with(String title, String description,
                                              String positiveText, DialogInterface.OnClickListener positiveClickListener,
                                              String negativeText, DialogInterface.OnClickListener negativeClickListener){

        this.title = title;
        this.description = description;
        this.positiveText = positiveText;
        this.positiveClickListener = positiveClickListener;
        this.negativeText = negativeText;
        this.negativeClickListener = negativeClickListener;

        return this;
    }

    /**
     *
     * The PermissionDialog let the user know, in a gentle way, why we will be asking for permissions
     */

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return new AlertDialog.Builder(getContext())
                .setTitle(title)
                .setMessage(description)
                .setPositiveButton(positiveText, positiveClickListener)
                .setNegativeButton(negativeText, negativeClickListener)
                .show();
    }

}